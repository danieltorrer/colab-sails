/**
 * BoxController
 *
 * @description :: Server-side logic for managing Box
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

 module.exports = {

 	create: function (req, res, next){

 		Box.create( req.params.all(), function boxCreated(err, box){
			// If Error
			if(err){
				console.log(err.toJSON());
				console.log(err);
				//console.log(err.invalidAttributes);
				
				req.session.flash = {
					err: err.invalidAttributes,
					type: 'warning'
				};

				return res.redirect('/');
			}

			//req.session.authenticated = true;
			//req.session.User = user

			//success
			//res.json(user);
			req.session.flash = {
				err: ["Box created"],
				type: 'success'
			}

			res.redirect('/box/'+box.url)
		})
 	},

 	find: function(req, res, next){
 		Box.findOne({ url: req.param('url')}, function foundUser(err, box){
 			if (err) return next(err);
 			if (!box) return next();
 			res.view({
 				box: box
 			})
 		})
 	}

 };

