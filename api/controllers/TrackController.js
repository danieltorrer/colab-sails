/**
 * TracksController
 *
 * @description :: Server-side logic for managing Tracks
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

 module.exports = {
 	create: function (req, res, next){

 		Box.create( req.params.all(), function userCreated(err, user){

			// If Error
			if(err){
				//console.log(err.toJSON());
				//console.log(err);
				//console.log(err.invalidAttributes);
				
				req.session.flash = {
					err: err.invalidAttributes,
					type: 'warning'
				};

				return res.redirect('/box/'+Box.url);
			}

			//req.session.authenticated = true;
			//req.session.User = user

			//success
			//res.json(user);
			req.session.flash = {
				err: ["Cuenta creada, ahora puedes iniciar sesión"],
				type: 'success'
			}

			res.redirect('/login')
		})
 	},
 };

