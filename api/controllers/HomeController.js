/**
 * HomeController
 *
 * @description :: Server-side logic for managing Home
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	index: function(req, res){

		if (req.isAuthenticated()) {
			res.view('user/show', {user: req.user[0]})
		}

		else{
			res.view('homepage')
		}

	}

};