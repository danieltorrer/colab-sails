var passport = require('passport');

module.exports = {

    login: function(req, res) {
        res.view();
    },
    
    process: function(req, res) {
        passport.authenticate('local', function(err, user, info) {
            if( (err) || (!user) ) {
                //console.log(info)
                req.session.flash = {
                    err:  [info.message],
                    type: 'warning'
                };

                return res.redirect('/login');
            }

            req.logIn(user, function(err) {
                if(err) res.send(err);
                
                return res.redirect('/')
                /*return res.send({
                    message: 'login successful'
                });*/
            });
        }) (req, res);
    },

    logout: function(req, res) {
        req.logOut();
        return res.redirect('/');
        //res.send('logout successful');
    }
};