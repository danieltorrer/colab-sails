/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

 module.exports = {

 	/*index: function(req, res){

 		User.find(function foundUser(err, users){
 			if (err) return res.serverError(err);
 			res.view({users: users})
 		})
 	},*/

 	edit: function(req, res){

 		User.findOne(req.param('id'), function foundUser (err, user){
 			if (err) return res.serverError(err);
 			if (!user) return res.serverError(err);


 			res.view({
 				user: user
 			});
 		})
 	},

 	update: function(req, res){
 		//console.log(req.params.all());
 		User.update(req.param('id'), req.params.all(), function userUpdated(err){
 			if (err) {
 				return res.redirect('/users/edit/'+ req.param('id'));
 			}
 			res.redirect('/users/show/'+ req.param('id'));
 		})
 	},

 	new: function (req, res){
 		res.view();
 	},

 	create: function (req, res, next){

 		User.create( req.params.all(), function userCreated(err, user){

			// If Error
			if(err){
				//console.log(err.toJSON());
				//console.log(err);
				//console.log(err.invalidAttributes);
				
				req.session.flash = {
					err: err.invalidAttributes,
					type: 'warning'
				};

				return res.redirect('/signup');
			}

			//req.session.authenticated = true;
			//req.session.User = user

			//success
			//res.json(user);
			req.session.flash = {
				err: ["Cuenta creada, ahora puedes iniciar sesión"],
				type: 'success'
			}

			res.redirect('/login')
		})
 	},

 	show: function (req, res, next){
 		User.findOne({ username: req.param('username')}, function foundUser(err, user){
 			if (err) return next(err);
 			if (!user) return next();
 			res.view({
 				user: user
 			})
 		})
 	},

 	destroy: function (req, res, next){
 		User.findOne(req.param('id'), function foundUser (err, user){
 			if (err) return next(err);

 			if (!user) return next('El usuario no existe');

 			User.destroy(req.param('id'), function userDestroyed(err){
 				if (err) return next(err);
 			});

 			res.redirect('/users');
 		})
 	}
 };
