/**
* Report.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	tableName: 'report',

	schema: true,

	attributes: {

		titulo: {
			type: 'string',
			required: true
		},

		description: {
			type: 'string'
		},

		imagen: {
			type: 'string',
			url: true
		},

		latitud: {
			type: 'string'
		},

		longitud: {
			type: 'string'
		}

	}
};

