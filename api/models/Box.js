/**
* Box.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var shortid = require('shortid');

module.exports = {

	schema: true,

	attributes: {

		name: {
			type: 'string',
			required: true
		},

		url: {
			type: 'string',
			unique: true
		},

		owner: {
			model: 'user'
		},

		songs: {
			collection: 'track',
			via: 'box'
		}
	},

	beforeCreate: function(box, cb) {
		url = shortid.generate() + shortid.generate()
		box.url = url.substring(2,10)
		
		cb();


		
	}

};

