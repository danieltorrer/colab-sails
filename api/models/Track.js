/**
* Tracks.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {

		name: {
			type: 'string',
			required: true
		},

		url: {
			type: 'string'
		},

		owner: {
			type: 'string'
		},

		img: {
			type: 'json'
		},

		box: {
			model: 'box'
		}
	}
};

