/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var bcrypt = require('bcryptjs');

module.exports = {
	tableName: 'user',

	types: {
		password: function(password) {
			return password === this.passwordConfirmation;
		}
	},

	schema: true,

	attributes: {
		
		username: {
			type: 'string',
			required: true,
			unique: true,
			alphanumeric: true
		},
		
		profilePic: {
			type: 'string',
			url: true
		},


		email: {			
			type: 'string',
			required: true,
			unique: true,
			email: true
		},

		password: {
			type: 'string',
			//minLength: 6,
			protected: true
		},

		boxes: {
			collection: 'box',
			via: 'owner'
		}

		/*report: {
			collection: 'report',
			via: 'user'
		}*/
	},

	beforeCreate: function(user, cb) {
		bcrypt.genSalt(10, function(err, salt) {
			bcrypt.hash(user.password, salt, function(err, hash) {
				if(err) {
					//console.log(err);
					cb(err);
				} else {
					user.password = hash;
					//console.log(hash);
					cb(null, user);
				}
			});
		});
	}

}