var passport = require('passport'),
LocalStrategy = require('passport-local').Strategy,
bcrypt = require('bcryptjs');

passport.serializeUser(function(user, done) {
	done(null, user.id);
});

passport.deserializeUser(function(id, done) {
	User.findById(id, function(err, user) {
		done(err, user);
	});
});

passport.use(new LocalStrategy({
	usernameField: 'email',
	passwordField: 'password'
},

//email y Password son los nombres de los campos a buscar en la BD
function(email, password, done) {
	User.findOne({ email: email }).exec(function(err, user) {
		if(err) {
			return done(err); 
		}
		if(!user) {
			return done(null, false, { message: 'There is no email ' + email }); 
		}

		bcrypt.compare(password, user.password, function(err, res) {
			if(!res) return done(null, false, {message: 'Email and password wrong'});
			return done(null, user);
		});
	});
}
));